package com.example.a313273.coinsk

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList


class MainActivity : AppCompatActivity() {

    var rvCoins: RecyclerView
    var coinAdapter: CoinAdapter
    var coins: List<Coin>
    var context: Context


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_main)
        rvCoins = rv_coins
        coins = ArrayList<Coin>()
        context = this
        coinAdapter = CoinAdapter(context, coins)

        rvCoins.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvCoins.adapter = coinAdapter


    }
}