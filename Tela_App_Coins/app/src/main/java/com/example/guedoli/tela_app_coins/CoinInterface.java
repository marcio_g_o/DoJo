package com.example.guedoli.tela_app_coins;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public interface CoinInterface {
    @GET("ticker")
    Call<List<Coin>> getCoins();

    class Creator {
        public static CoinInterface newService() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.coinmarketcap.com/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(CoinInterface.class);

        }

    }

}
