package com.example.guedoli.tela_app_coins;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CoinAdapter extends RecyclerView.Adapter<CoinAdapter.CoinHolde> {

    private LayoutInflater layoutInflater;

    public CoinAdapter(Context context, List<Coin> coins) {
    }

    @NonNull
    @Override
    public CoinHolde onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.adpatercoin, parent,false);
        return new CoinHolde(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoinHolde holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class CoinHolde extends RecyclerView.ViewHolder
    {
        ImageView item1;
        TextView texto3;
        TextView texto5;
        TextView texto7;
        TextView texto9;


        public CoinHolde(View itemView) {
            super(itemView);
            item1 = itemView.findViewById(R.id.item1);
            texto3 = itemView.findViewById(R.id.texto3);
            texto5 = itemView.findViewById(R.id.texto5);
            texto7 = itemView.findViewById(R.id.texto7);
            texto9 = itemView.findViewById(R.id.texto9);

        }
    }

}
